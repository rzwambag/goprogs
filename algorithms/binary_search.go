package algorithms

// LinearSearch uses a linear search algorithm to find the wanted integer inside
// of the given slice.
func LinearSearch(haystack []int, needle int) int {
	for i := 0; i < len(haystack); i++ {
		if haystack[i] == needle {
			return i
		}
	}
	return -1
}

// BinarySearch uses a binary search algorithm to find the wanted integer inside
// of hte given slice.
func BinarySearch(haystack []int, needle int) int {
	return -1
}

// TernarySearch uses a binary ternary search algorithm to find the wanted
// integer inside of the given slice.
func TernarySearch(haystack []int, needle int) int {
	return -1
}

// MetaBinarySearch uses a meta binary search algorithm to find the wanted
// integer inside of the given slice.
func MetaBinarySearch(haystack []int, needle int) int {
	return -1
}
