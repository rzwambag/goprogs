package mechanics

// Side represents the side, left of right, of something
type Side int

const (
	// Left is like the right, but that way <-
	Left Side = iota + 1
	// Right is like the left, but that way ->
	Right
)

// NextSide is a function that tells RSplitSlice which direction to go next
type NextSide func() Side

func (s Side) String() string {
	switch s {
	case Left:
		return "left"
	case Right:
		return "right"
	}
	panic("invalid side")
}

// RSplitSlice resursively splits a slice in a specified direction until there
// is only one element remaining. You will determine the proper direction to
// to split the slice by calling the provided function ns.
//
// For example, the first time you call ns it may yield left. This means you
// must split the slice and recursively operate on the left side. If the next
// time you call ns it yields right, then that means you must split the right
// side of the remaining slice (the original left half). This splitting
// continues until the base condition is reach: one element left.
//
// Parameter s will always be a non-empty slice
//
// Parameter ns will return a finite number of directions. Accessing it more
// times than necessary will cause the test to fail.
func RSplitSlice(s []int, ns NextSide) int {
	if len(s) == 1 {
		return s[0]
	}

	if ns() == Left {
		return RSplitSlice(s[:len(s)/2], ns)
	} else { // Right
		return RSplitSlice(s[len(s)/2:], ns)
	}
}
