package mechanics

import (
	"errors"
)

// MiddleElement returns the middle element of the provided slice. An error is
// returned if the slice is empty. If the slice has an even number of elements,
// it favors the left side of the midpoint.
func MiddleElement(s []int) (int, error) {
	if len(s) == 0 {
		return 0, errors.New("Slice can not be of length 0")
	}

	if len(s)%2 == 0 {
		return s[(len(s)/2)-1], nil
	} else {
		return s[(len(s) / 2)], nil
	}
}
